# Patrón arquitectónico MVC en sistema de votaciones

### Diagrama de clases

En azul se resaltan las clases que pertecen al patrón arquitectónico.

![Diagrama de clases](https://svgshare.com/i/SFv.svg)

### Instalación

Para poder desarrollar tu proyecto utilizando este patrón puedes clonar el repositorio del proyecto desde el siguiente link: https://gitlab.com/alanmoraales/mvc-implementation-votes-system.

Esto descargará el proyecto de ejemplo usando MCV en un sistema de votaciones, el cual usa también gradle para construir el proyecto y manejar las dependencias. A partir de aquí puedes hacer varias cosas:

1. Mantener gradle, mantener la estructura de directorios, eliminar las clases que no pertenecen al patrón y comenzar con tu proyecto.
2. Mantener gradle, Cambiar la estructura de directorios, eliminar las clases que no pertenecen al patrón y comenzar con tu proyecto.
3. No utilizar gradle y construir un proyecto desde cero. En este caso solo necesitaras mover la carpeta mvc dentro de src/main/java/ a tu nuevo proyecto y estaras listo.

En resumen, instalar esta implementación en tu proyecto solo necesitas copiar la carpeta src/main/java/mvc.

### Uso

##### Definiendo un modelo

Para crear un nuevo modelo solo debes crear un clase y extender de Model.

````java
public class Candidate extends Model {
	// tus atributos y métodos
}
````

Cuando extiendes de Model, heredas una función llamada sendUpdateNotification(). Esta se encarga de avisar a las vistas enlazadas al modelo que se ha actualizado la información. Lamentablemente, tendrás que llamar a estar función de forma manual cada que actualices información de interés para tus vistas.

````java
// Candidate.java
public void setVotesCount(int votesCount) {
    this.votesCount = votesCount;
    // ejecutamos la función para que la vista se actualice cada que cambian los votos.
    this.sendUpdateNotification();
}
````

##### Definiendo una vista

Las vistas son clases parametrizadas. Para crear una debes crear un clase, extender de View, y especificar el modelo y controlador a utilizar.

````java
public class PieChartView extends View<Candidate, CandidateController> {
	// tus atributos y métodos
    @Override
    public void updateData() {
		// esto se ejecuta cada vez que se actualiza el modelo
    }
}
````

Al extender de View, tendrás que implementar la función updateData(), la cual se ejecutará cada vez que se actualice el modelo. Igualmente, tendrás acceso al array de modelos, al controlador y las funciones addModel() y setModelController(), estas últimas servirán para crear las relaciones entre las clases.

Es importante especificar el modelo y el controlador a utilizar, porque de esta manera podrás tener acceso a las funciones que definas dentro de las clases que hayas creado. Para utilizar los modelos, tendrás que hacerlo a través de "models".

````java
// PieChartView.java
@Override
public void updateData() {
	for(var candidate : models) {
    	pieChartFrame.setVotesFor(candidate.getName(), 	candidate.getVotesCount());
        // getName() y getVotesCount() son funciones de la clase Candidate.
	}
}
````

Para utilizar los métodos del controlador debes accerder a él a través de "modelController".

````java
// PollView.java
public void addVote(String candidateName) {
    // addOneVoteFor() es una función de CandidateController
    this.modelController.addOneVoteFor(candidateName);
}
````

##### Definiendo un controlador

Debes crear un clase, extender de Controller y especificar el modelo al cual quieres acceder.

````java
public class CandidateController extends Controller <Candidate> {
	// tus atributos y métodos aquí
}
````

Puedes acceder a los modelos a través del array "models", el cual estará disponible apenas extiendas de Controller. Cabe resaltar que, al igual que en las vistas, una vez especifiques el tipo del modelo, solo podrás añadir modelos de ese tipo. Para añadir un modelo al controlador, puedes utilizar la función addModel().

````java
// accediendo a los modelos desde el controlador
public void addOneVoteFor(String candidateName) {
    var candidate = findCandidateByName(candidateName);
    int currentVotesCount = candidate.getVotesCount();
    candidate.setVotesCount(currentVotesCount + 1);
}
````

##### Creando los objetos y las asociaciones.

Para añadir modelos a un controlador o a una vista puedes utilizar la función addModel. El ejemplo de abajo muestra como creamos una nueva instancia del modelo Candidate y se las añadimos al controlador y a la vista.

Adicionalmente,  al vista se le puede añadir un controlador, para ellos utilizarás la función addModelController como se ve abajo.

````java
// creando instancia del modelo
var candidate1 = new Candidate();
candidate1.setVotesCount(0);
candidate1.setName("Candidate 1");

// pasando la instancia del modelo a una instancia del controlador
var candidateController = new CandidateController();
candidateController.addModel(candidate1);

// pasando la instancia del modelo y del controlador a la vista
var pieChartView = new PieChartView();
pieChartView.addModel(candidate1);
pieChartView.addModelController(candidateController);
````



 

