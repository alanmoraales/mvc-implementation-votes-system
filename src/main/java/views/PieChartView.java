package views;

import controllers.CandidateController;
import models.Candidate;
import mvc.View;

public class PieChartView extends View<Candidate, CandidateController> {
  private PieChartFrame pieChartFrame;

  public PieChartView() {
    this.pieChartFrame = new PieChartFrame();
  }

  public void display() {
    pieChartFrame.setVisible(true);
  }

  @Override
  public void updateData() {
    for(var candidate : models) {
      pieChartFrame.setVotesFor(candidate.getName(), candidate.getVotesCount());
    }
  }
}