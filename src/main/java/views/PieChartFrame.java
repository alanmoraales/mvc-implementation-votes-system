package views;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import java.awt.*;

public class PieChartFrame extends JFrame {
  private DefaultPieDataset plotData;

  public PieChartFrame() {
    var dataset = createDataset();
    var chart = createChart(dataset);
    var chartPanel = new ChartPanel(chart);

    chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    chartPanel.setBackground(Color.white);
    add(chartPanel);

    pack();
    setTitle("Pie Chart");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public void setVotesFor(String candidate, int votesCount) {
    plotData.setValue(candidate, votesCount);
  }

  private DefaultPieDataset createDataset() {
    plotData = new DefaultPieDataset();
    plotData.setValue("Candidate 1", 0);
    plotData.setValue("Candidate 2", 0);
    plotData.setValue("Candidate 3", 0);

    return plotData;
  }

  private JFreeChart createChart(DefaultPieDataset dataset) {
    return ChartFactory.createPieChart("Votes Count", dataset, false, true, false);
  }
}
