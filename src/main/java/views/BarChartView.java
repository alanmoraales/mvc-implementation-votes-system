package views;

import controllers.CandidateController;
import models.Candidate;
import mvc.View;

public class BarChartView extends View<Candidate, CandidateController> {
  private final BarChartFrame barChartFrame;

  public BarChartView() {
    this.barChartFrame = new BarChartFrame();
  }

  public void display() {
    this.barChartFrame.setVisible(true);
  }

  @Override
  public void updateData() {
    for(var candidate : models) {
      barChartFrame.setVotesFor(candidate.getName(), candidate.getVotesCount());
    }
  }
}
