package views;

import controllers.CandidateController;
import models.Candidate;
import mvc.View;

public class PollView extends View<Candidate, CandidateController> {
  private final PollViewFrame pollViewFrame;

  public PollView() {
    this.pollViewFrame = new PollViewFrame();
    this.pollViewFrame.setPollViewMethods(this);
  }

  public void addVote(String candidateName) {
    this.modelController.addOneVoteFor(candidateName);
  }

  public void display() {
    pollViewFrame.setVisible(true);
  }

  @Override
  public void updateData() {
    // nothing to update here
  }
}
