package views;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;

public class BarChartFrame extends JFrame {
  private DefaultCategoryDataset plotData;

  public BarChartFrame() {
    var dataset = createDataset();
    var chart = createChart(dataset);
    var chartPanel = new ChartPanel(chart);

    chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    chartPanel.setBackground(Color.white);
    add(chartPanel);

    pack();
    setTitle("Pie Chart");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public void setVotesFor(String candidate, int votesCount) {
    plotData.setValue(votesCount, "Votes", candidate);
  }

  private CategoryDataset createDataset() {
    plotData = new DefaultCategoryDataset();
    plotData.setValue(0, "Votes", "Candidate 1");
    plotData.setValue(0, "Votes", "Candidate 2");
    plotData.setValue(0, "Votes", "Candidate 3");

    return plotData;
  }

  private JFreeChart createChart(CategoryDataset dataset) {
    return ChartFactory.createBarChart("Votes Count", "", "Votes Count", dataset, PlotOrientation.VERTICAL, true, true, false);
  }
}
