package views;

import javax.swing.*;

public class PollViewFrame extends JFrame {
  private PollView pollViewMethods;

  public PollViewFrame() {
    JPanel rootPanel = new JPanel();
    JButton voteForCandidateOneButton = new JButton();
    JButton voteForCandidateTwoButton = new JButton();
    JButton voteForCandidateThreeButton = new JButton();

    voteForCandidateOneButton.setText("VOTE FOR CANDIDATE ONE");
    voteForCandidateOneButton.addActionListener(e -> this.pollViewMethods.addVote("Candidate 1"));

    voteForCandidateTwoButton.setText("VOTE FOR CANDIDATE TWO");
    voteForCandidateTwoButton.addActionListener(e -> this.pollViewMethods.addVote("Candidate 2"));

    voteForCandidateThreeButton.setText("VOTE POR CANDIDATE THREE");
    voteForCandidateThreeButton.addActionListener(e -> this.pollViewMethods.addVote("Candidate 3"));

    rootPanel.add(voteForCandidateOneButton);
    rootPanel.add(voteForCandidateTwoButton);
    rootPanel.add(voteForCandidateThreeButton);
    this.add(rootPanel);
  }

  public void setPollViewMethods(PollView pollViewMethods) {
    this.pollViewMethods = pollViewMethods;
  }
}
