package controllers;

import models.Candidate;
import mvc.Controller;

public class CandidateController extends Controller <Candidate> {
  public void addOneVoteFor(String candidateName) {
    var candidate = findCandidateByName(candidateName);
    int currentVotesCount = candidate.getVotesCount();
    candidate.setVotesCount(currentVotesCount + 1);
  }

  private Candidate findCandidateByName(String name) {
    for(var candidate : this.models) {
      if(candidate.getName().equals(name)) {
        return candidate;
      }
    }
    return null;
  }
}
