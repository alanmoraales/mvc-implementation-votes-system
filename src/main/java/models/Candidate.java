package models;

import mvc.Model;

public class Candidate extends Model {
  private String name;
  private int votesCount;

  public void setName(String name) {
    this.name = name;
  }

  public void setVotesCount(int votesCount) {
    this.votesCount = votesCount;
    this.sendUpdateNotification();
  }

  public String getName() {
    return name;
  }

  public int getVotesCount() {
    return this.votesCount;
  }
}
