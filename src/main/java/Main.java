import controllers.CandidateController;
import models.Candidate;
import views.BarChartView;
import views.PieChartView;
import views.PollView;

public class Main {
  public static void main(String[] args) {
    var candidate1 = new Candidate();
    candidate1.setVotesCount(0);
    candidate1.setName("Candidate 1");

    var candidate2 = new Candidate();
    candidate2.setVotesCount(0);
    candidate2.setName("Candidate 2");

    var candidate3 = new Candidate();
    candidate3.setVotesCount(0);
    candidate3.setName("Candidate 3");

    var candidateController = new CandidateController();
    candidateController.addModel(candidate1);
    candidateController.addModel(candidate2);
    candidateController.addModel(candidate3);

    var pieChartView = new PieChartView();
    pieChartView.addModel(candidate1);
    pieChartView.addModel(candidate2);
    pieChartView.addModel(candidate3);

    var barCharView = new BarChartView();
    barCharView.addModel(candidate1);
    barCharView.addModel(candidate2);
    barCharView.addModel(candidate3);

    var pollView = new PollView();
    pollView.setModelController(candidateController);

    pollView.display();
    pieChartView.display();
    barCharView.display();
  }
}
