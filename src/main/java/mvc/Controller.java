package mvc;

import java.util.ArrayList;

public abstract class Controller<M extends Model> {
  protected ArrayList<M> models;

  public Controller() {
    this.models = new ArrayList<>();
  }

  public void addModel(M model) {
    this.models.add(model);
  }
}
