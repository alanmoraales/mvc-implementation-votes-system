package mvc;

import java.util.ArrayList;

public abstract class View<M extends Model, C extends Controller> implements ModelObserver {
  protected ArrayList<M> models;
  protected C modelController;

  public View() {
    this.models = new ArrayList<>();
  }

  public void addModel(M model) {
    model.addObserver(this);
    this.models.add(model);
  }

  public void setModelController(C modelController) {
    this.modelController = modelController;
  }
}
