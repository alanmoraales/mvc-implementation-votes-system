package mvc;

import java.util.ArrayList;

public abstract class Model {
  private ArrayList<ModelObserver> observers;

  public Model() {
    this.observers = new ArrayList<>();
  }

  protected void sendUpdateNotification() {
    for(ModelObserver observer : observers) {
      observer.updateData();
    }
  }

  public void addObserver(ModelObserver observer) {
    this.observers.add(observer);
  }

  public void removeObserver(ModelObserver observer) {
    this.observers.remove(observer);
  }
}
